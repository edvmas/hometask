pragma solidity >=0.4.25 <0.6.0;

contract HomeTask {

  uint[] sums;

  function add(uint a, uint b) public returns (uint c) {
    c = a + b;
    assert(c >= a);
    sums.push(c);
  }

  uint c = add(1, 15);

  function returnSums() public view returns(uint[] memory){
    return sums;
  }

}
